#ifndef FRACTION_HPP_INCLUDED
#define FRACTION_HPP_INCLUDED

///only represents non-mixed fractions!
#include <cstdint>
#include <cmath>
#include <type_traits>
#include <iostream>

namespace Maths {
    struct Fraction {

        int64_t numerator, denominator;

        ///ctor
        Fraction(int64_t, int64_t);
        Fraction(double);
        Fraction();

        ///copy ctor
        Fraction(const Fraction&);

        ///@return simplified version of fraction
        Fraction simplified() const;

        ///simplifies fraction
        Fraction simplify();

        ///@return Fraction(denominator / numerator)
        Fraction reciprocal();

        ///@return numerator / denominator
        double toDecimal() const;

        /**operator overloads (all operations are simplified)
        -----------------------------------------------------------------------------------------------------------------------------**/
        ///+
        const Fraction operator+(const Fraction&);
        const Fraction operator+(const int64_t&);

        ///-
        const Fraction operator-(const Fraction&);
        const Fraction operator-(const int64_t&);

        ///divide
        const Fraction operator/(const Fraction&);
        const Fraction operator/(const int64_t&);

        ///*
        const Fraction operator*(const Fraction&);
        const Fraction operator*(const int64_t&);

        ///+=
        Fraction &operator+=(const Fraction&);
        Fraction &operator+=(const int64_t&);

        ///-=
        Fraction &operator-=(const Fraction&);
        Fraction &operator-=(const int64_t&);

        ///divide=
        Fraction &operator/=(const Fraction&);
        Fraction &operator/=(const int64_t&);

        ///*=
        Fraction &operator*=(const Fraction&);
        Fraction &operator*=(const int64_t&);

        ///=
        Fraction &operator=(const Fraction&);
        ///=(decimal) - NOTE: will not work properly for numbers with ore than 3 decimal points.
        Fraction &operator=(const double&);

        ///==
        bool operator==(const Fraction&);
        bool operator==(const int64_t&);

        ///!=
        bool operator!=(const Fraction&);
        bool operator!=(const int64_t&);

        ///>
        bool operator>(const Fraction&);
        bool operator>(const int64_t&);

        ///<
        bool operator<(const Fraction&);
        bool operator<(const int64_t&);

        ///>=
        bool operator>=(const Fraction&);
        bool operator>=(const int64_t&);

        ///<=
        bool operator<=(const Fraction&);
        bool operator<=(const int64_t&);
        ///---------------------------------------------------------------------------------------------------------------------------

        ///ostream<< (friendship declaration)
        friend std::ostream &operator<<(std::ostream&, const Fraction&);
    };
}

#endif // FRACTION_HPP_INCLUDED
