#ifndef MATHS_H_INCLUDED
#define MATHS_H_INCLUDED

#include "Fraction.hpp"
#include "Angle.h"
#include "Matrix.h"
#include <cmath>

///@author Jarrett Kastler

///This library depends on C++11 and STL.

///Mathematical structures currently represented:
/// 2D Vectors (may be expanded to N-dimensional vectors)
/// Fractions
/// Angles
/// Matrices

///This file contains general functions for mathematical operations and formulas
///As well as values for relevant mathematical constants

namespace Maths {

    ///always pass angles in radians for function in this namespace.
    /** Vectors have two common forms:
        (X, Y) - representing a point in 2d space.
        (A, L) - representing an angle and magnitude of motion.

        all functions in this namespace expect the (X, Y) form.
    **/

    ///functions for converting between degrees and radians.
    double toDegrees(double);
    double toRadians(double);

    ///constants
    long double pi();
    double epsilon();
    long double phi();

    /**@return
        -1: A < B
        0:  A == B
        1:  A > B
    **/
    int8_t compare(double, double);

    ///@return the distance between two points in 2D space.
    template <typename Ta, typename Tb>
    double distance(Vector2<Ta> a, Vector2<Tb> b) {
        return sqrt(pow(b.x - a.x, 2) + pow(b.y - a.y, 2));
    }

    ///@return angle of (X, Y) form vector.
    template <typename T>
    double angle(Vector2<T> v) {
        return atan2(v.y, v.x);
    }

    ///@return angle of line between two points in 2D space.
    template <typename Ta, typename Tb>
    double angle(Vector2<Ta> a, Vector2<Tb> b) {
        return atan2(b.y - a.y, b.x - a.x);
    }

    ///@param (A) angle in radians
    ///@return unit vector representing A
    Vector2<double> toVector(double);

    /**@param (R) the radical coefficient
    ***@param (N) the number to go under the radical
    ***@return N^(1/R)
    **/
    double root(double, double);

    ///@return greatest common factor of both params.
    int64_t gcf(int64_t, int64_t);

    ///slope of a line between 2 points in 2D space.
    template<typename T1, typename T2>
    double slope(Vector2<T1> a, Vector2<T2> b) {
        double slopeD = (b.y - a.y) / (b.x - a.x);
        return slopeD;
    }
    ///multiply Vector2 by Matrix
    template<typename T>
    const Matrix operator*(Matrix m, const Vector2<T> &v) {
        Matrix m2({(double)v.x}, {(double)v.y});
        return m * m2;
    }

};

namespace math = Maths;
namespace maths = math;

#endif // MATHS_H_INCLUDED
