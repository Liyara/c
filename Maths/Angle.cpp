#include "Angle.h"
#include "Maths.h"

Angle::Angle() {
    raw = 0;
}

Angle::Angle(double r) {
    raw = r;
}

Angle::Angle(Vector2<double> v) {
    setVector(v);
}

double Angle::asRadians() const {
    return raw;
}

double Angle::asDegrees() const {
    return raw * (double)180 / Maths::pi();
}

Vector2<double> Angle::asVector() const {
    return Vector2<double>(cos(raw), sin(raw));
}

Angle Angle::setRadians(double r) {
    raw = r;
    return *this;
}

Angle Angle::setDegrees(double d) {
    raw = d * Maths::pi() / 180;
    //std::cout << 180 / Maths::pi() << std::endl;
    return *this;
}

Angle Angle::setVector(Vector2<double> v) {
    raw = atan2(v.y, v.x);
    return *this;
}

auto Angle::getType() const -> Type {
    double degs = fabs(asDegrees());
    while (degs > 360) {
        degs -= 360;
    }
    if (degs < 90) {
        return Type::ACUTE;
    } else if (degs == 90) {
        return Type::RIGHT;
    } else if (degs > 90 && degs < 180) {
        return Type::OBTUSE;
    } else if (degs == 180) {
        return Type::STRAIGHT;
    } else {
        return Type::REFLEX;
    }
}

Angle Angle::bisector() const {
    return Angle(raw / (double)2);
}

Angle Angle::inverse() const {
    double r = fabs(raw * 3);
    while (r > Maths::toRadians(360)) {
        r -= Maths::toRadians(360);
    }
    return Angle(r);
}
