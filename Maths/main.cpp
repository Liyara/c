#include <iostream>
#include "Maths.h"
#include <typeinfo>

///n * (-2 * (v * n)) + v

int main() {
    math::Matrix::readErrorsTo(std::cout);
    math::Matrix m ({
        {0, 2},
        {3, 4}
    });
    math::Matrix mm({
        {4, 2},
        {5, 6}
    });
    std::cout << (m - mm) << std::endl;
    uint8_t e = 0;
    return 0;
}
