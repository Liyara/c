#include "Maths.h"

namespace Maths {
    const long double PI = acosl(-1.0L), PHI = (1 + sqrt(5)) / 2;
    const double EPSILON = 0.000001;
    double toDegrees(double rads) {
        return rads * 180 / PI;
    }
    double toRadians(double degs) {
        return degs * PI / 180;
    }
    long double pi() {
        return PI;
    }
    double epsilon() {
        return EPSILON;
    }
    long double phi() {
        return PHI;
    }
    int8_t compare(double a, double b) {
        double d = fabs(a - b);
        if (d < epsilon()) {
            return 0;
        } else if (d > epsilon() && a < b) {
            return -1;
        } else if (d > epsilon() && a > b) {
            return 1;
        }
    }
    Vector2<double> toVector(double a) {
        Vector2<double> result(cos(a), sin(a));
        return result;
    }
    double root(double r, double n) {
        if (n < 0) {
            return -root(r, -n);
        } else {
            return pow(n, 1/r);
        }
    }
    int64_t gcf(int64_t a, int64_t b) {
        int l = a, r  = b, an = l % r;
        if (an == 0) return r;
        while (std::abs(an) > 0) {
        	l = an;
            an = r % an;
            r = l;
        }
        return l;
    }
}
