#ifndef VECTOR_H_INCLUDED
#define VECTOR_H_INCLUDED

///no accompanying .cpp file due to high template content.
///Vector2 is a MATHEMATICAL structure, and is meant only to mirror the properties of 2d mathematical vectors.
/** To use an object of a user-defined class with Vector2 properly,
    the class should have operator overloads for all primitives,
    as well as all other user-defined classes with which you intend to use Vector2.
**/
#include <cstdint>
#include <cmath>
#include <type_traits>
#include <iostream>
#include <initializer_list>

namespace Maths {
    template<
        typename T,
        typename = typename std::enable_if<std::is_arithmetic<T>::value, T>::type
    >
    struct Vector2 {

        T x, y;

        ///ctor
        Vector2() {
            x = (T)0;
            y = (T)0;
        }
        Vector2(T a, T b) : x(a), y(b) {}
        Vector2(T n) : x(n), y(n) {}
        Vector2(std::initializer_list<T> items) : x(*items.begin()), y(*(items.begin() + 1)) {}

        ///copy ctor
        template <typename U>
        Vector2(const Vector2<U> &other) : x(other.x), y(other.y) {}

        ///@return *this / ||*this||
        Vector2<double> unitForm() const {
            return *this / magnitude();
        }

        ///@return ||*this||
        double magnitude() const {
            return sqrt(pow(x, 2) + pow(y, 2));
        }

        /**operator overloads
        -----------------------------------------------------------------------------------------------------------------------------**/
        ///+
        template<typename U>
        auto operator+(const Vector2<U> &b) const -> Vector2<decltype(x + b.x)> {
            Vector2<decltype(x + b.x)> result(x + b.x, y + b.y);
            return result;
        }

        ///-
        template<typename U>
        auto operator-(const Vector2<U> &b) const -> Vector2<decltype(x - b.x)> {
            Vector2<decltype(x - b.x)> result(x + (b.x * -1), y + (b.y * -1));
            return result;
        }

        ///divide
        template<typename U>
        auto operator/(const U &b) const -> Vector2<decltype(x / b)> {
            Vector2<decltype(x / b)> result(x / b, y / b);
            return result;
        }
        template<typename U>
        auto operator/(const Vector2<U> &b) const -> Vector2<decltype(x / b.x)> {
            Vector2<decltype(x / b.x)> result(x / b.x, y / b.y);
            return result;
        }

        ///*
        template<typename U>
        auto operator*(const U &n) const -> Vector2<decltype(x * n)> {
            Vector2<decltype(x * n)> result(x * n, y * n);
            return result;
        }
        template<typename U>
        auto operator*(const Vector2<U> &b) const -> decltype(x * b.x + y * b.y) {
            return x * b.x + y * b.y;
        }

        ///+=
        template<typename U>
        Vector2<T> &operator+=(const Vector2<U> &v) {
            x += v.x;
            y += v.y;
            return *this;
        }


        ///-=
        template<typename U>
        Vector2<T> &operator-=(const Vector2<U> &v) {
            Vector2 v2(v.x * -1, v.y * -1);
            x += v2.x;
            y += v2.y;
            return *this;
        }


        ///divide=
        template <typename T2>
        Vector2<T> &operator/=(const T2 &n) {
            x /= n;
            y /= n;
            return *this;
        }
        template<typename U>
        Vector2<T> &operator/=(const Vector2<U> &v) {
            x /= v.x;
            y /= v.y;
            return *this;
        }

        ///*=
        Vector2<T> &operator*=(const T &n) {
            x *= n;
            y *= n;
            return *this;
        }

        ///=
        template<typename U>
        Vector2<T> &operator=(const Vector2<U> &v) {
            x = v.x;
            y = v.y;
            return *this;
        }
        Vector2 <T> &operator=(std::initializer_list<T> items) {
            x = *items.begin();
            y = *(items.begin() + 1);
            return *this;
        }

        ///booleans - BEWARE: these operators perform a basic == check, careful with floating points!

        ///==
        template<typename U>
        bool operator==(const Vector2<U> &v) {
            return (v.x == x && v.y == y);
        }

        ///!=
        template<typename U>
        bool operator!=(const Vector2<U> &v) {
            return !(*this == v);
        }
        ///---------------------------------------------------------------------------------------------------------------------------

        ///ostream<< (friendship declaration)
        template<typename U>
        friend std::ostream &operator<<(std::ostream &out, const Vector2<U> &cV);

        ///type casts
        template<typename U>
        operator Vector2<U>() {
            Vector2<U> result((U)x, (U)y);
            return result;
        }
    };

    ///typedefs
    typedef Vector2<int> Vector2i;
    typedef Vector2<unsigned> Vector2u;
    typedef Vector2<long> Vector2l;
    typedef Vector2<short> Vector2s;

    typedef Vector2<int8_t> Vector2i8;
    typedef Vector2<uint8_t> Vector2u8;

    typedef Vector2<int16_t> Vector2i16;
    typedef Vector2<uint16_t> Vector2u16;

    typedef Vector2<int32_t> Vector2i32;
    typedef Vector2<uint32_t> Vector2u32;

    typedef Vector2<int64_t> Vector2i64;
    typedef Vector2<uint64_t> Vector2u64;

    typedef Vector2<float> Vector2f;
    typedef Vector2<double> Vector2d;
    typedef Vector2<long double> Vector2ld;

    ///ostream<< (definition)
    template<typename U>
    std::ostream &operator<<(std::ostream &out, const Vector2<U> &cV) {
        out << "(" << cV.x << ", " << cV.y << ")";
        return out;
    }
}

#endif // VECTOR_H_INCLUDED
