#ifndef ANGLE_H
#define ANGLE_H
#include "Vector2.hpp"

using Maths::Vector2;

class Angle {
public:
    enum Type {
        ACUTE,
        RIGHT,
        OBTUSE,
        STRAIGHT,
        REFLEX
    };
    Angle();
    Angle(double); ///only accepts radians
    Angle(Vector2<double>);
    double asRadians() const;
    double asDegrees() const;
    Vector2<double> asVector() const;
    Angle setRadians(double);
    Angle setDegrees(double);
    Angle setVector(Vector2<double>);
    Type getType() const;
    Angle bisector() const;
    Angle inverse() const;
private:
    double raw; ///stored as radians
};

#endif // ANGLE_H
