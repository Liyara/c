#ifndef MATRIX_H
#define MATRIX_H

#include <deque>
#include <vector>
#include <cstdint>
#include <iostream>

///error codes
#define MATERR_OK                       0x00 ///@e No error
#define MATERR_INAVLID_OPERATION        0x01 ///@e arithmetic operation failed
#define MATERR_OUT_OF_BOUNDS            0x02 ///@e Got index not within bounds of matrix
#define MATERR_NON_SQUARE               0x04 ///@e Non-square matrix used in square-dependent operation
#define MATERR_INCOMPLETE               0x08 ///@e Initializer list provided for matrix initialization does not form complete rectangle

namespace Maths {

    ///forward
    class Matrix;
    class MatrixRow;

    ///typedef
    typedef std::initializer_list<double> MatRowLiteral;
    typedef uint8_t MatIndex;
    typedef std::initializer_list<MatRowLiteral> MatLiteral;

    struct MatrixRow {
        ///ctor, initializes as a 1x1 matrix {0}
        MatrixRow();

        ///@param [size]
        ///ctor, fills data with [size] copies of zero
        MatrixRow(MatIndex);

        ///copy ctor
        MatrixRow(const MatrixRow&);

        ///@param row data
        MatrixRow(const MatRowLiteral&);

        ///@param data [index]
        ///@return entry at position [index]
        const double &operator[](MatIndex) const;

        ///non-const
        double &operator[](MatIndex);

        ///@param data [index]
        ///@param [data]
        ///@return successful?
        ///sets entry at position [index] to [data]
        bool set(MatIndex, double);

        ///@return number of columns
        MatIndex cols() const;

        ///@return dot product of two rows
        const double operator*(const MatrixRow&);

        ///assignment operator
        MatrixRow &operator=(const MatrixRow&);

        ///==
        bool operator==(const MatrixRow&);

        //!=
        bool operator!=(const MatrixRow&);

        ///@param new [size]
        ///change size of data to [size]
        void resize(MatIndex);

        ///@return dot product of two rows
        static double dotProduct(const MatrixRow&, const MatrixRow&);


    private:
        std::vector<double> data;
    };

    class Matrix {
    public:
        ///@param [rows]
        ///@param [columns]
        ///ctor, creates matrix of size [rows]x[columns] filled with zeros
        Matrix(MatIndex, MatIndex);

        ///@param matrix data
        Matrix(const MatLiteral&);

        ///@param row [index]
        ///return row at position [index]
        const MatrixRow &operator[](MatIndex) const;

        ///non-const
        MatrixRow &operator[](MatIndex);

        ///@param column [index]
        ///return column at position [index]
        MatrixRow getColumn(MatIndex) const;

        ///non-const
        MatrixRow getColumn(MatIndex);

        ///@return number of rows
        MatIndex rows() const;

        ///@return number of cols
        MatIndex cols() const;

        ///@return transpose of matrix
        Matrix transpose() const;

        ///@return cofactor of matrix
        Matrix cofactor() const;

        ///@return determinant of matrix
        double determinant() const;

        ///@return inverse of matrix
        Matrix inverse() const;

        ///@param [row] to be excluded
        ///@param [column] to be excluded
        ///returns matrix without [row] and [column]
        Matrix subMatrix(MatIndex, MatIndex) const;

        ///@return has same number of rows and columns?
        bool square() const;

        ///@param [number] of rows and columns
        ///@return identity matrix of size [number]
        static Matrix identity(MatIndex);

        ///@return error on top of error stack
        ///deletes error returned from error stack
        static uint8_t readError();

        ///log error writes to an ostream
        static void readErrorsTo(std::ostream&);

        /**operator overloads
        -----------------------------------------------------------------------------------------------------------------------------**/
        ///+
        const Matrix operator+(const Matrix&);

        ///-
        const Matrix operator-(const Matrix&);

        ///*
        const Matrix operator*(const Matrix&);
        const Matrix operator*(double);

        ///+=
        Matrix &operator+=(const Matrix&);

        ///-=
        Matrix &operator-=(const Matrix&);

        ///*=
        Matrix &operator*=(const Matrix&);
        Matrix &operator*=(double);

        ///=
        Matrix &operator=(const Matrix&);

        ///==
        bool operator==(const Matrix&);

        ///!=
        bool operator!=(const Matrix&);
        ///---------------------------------------------------------------------------------------------------------------------------

        friend std::ostream &operator<<(std::ostream&, const Matrix&);
        friend class MatrixRow;

    private:
        static void writeError(uint8_t);
        std::deque<MatrixRow> data;
    };
}

#endif // MATRIX_H
