#include "Fraction.hpp"
#include "Maths.h"
#include <sstream>
#include <iomanip>

namespace Maths {
    Fraction::Fraction(int64_t a, int64_t b) : numerator(a), denominator(b) {}
    Fraction::Fraction() : numerator(1), denominator(1) {}
    Fraction::Fraction(const Fraction &other) : numerator(other.numerator), denominator(other.denominator) {}
    Fraction::Fraction(double d) {
        *this = d;
    }
    Fraction Fraction::simplify() {
        *this = simplified();
        return *this;
    }
    Fraction Fraction::simplified() const {
        Fraction r = *this;
        int64_t g = 0;
        g = gcf(numerator, denominator);
        r.numerator /= g;
        r.denominator /= g;
        return r;
    }
    const Fraction Fraction::operator+(const Fraction &b) {
        Fraction l = *this;
        l.numerator *= b.denominator;
        l.denominator *= b.denominator;
        Fraction r  = b;
        r.numerator *= denominator;
        r.denominator *= denominator;
        Fraction result;
        result.numerator = l.numerator + r.numerator;
        result.denominator = l.denominator;
        return result.simplified();
    }
    const Fraction Fraction::operator+(const int64_t &n) {
        return *this + Fraction(n, 1);
    }
    const Fraction Fraction::operator-(const Fraction &b) {
        Fraction l = *this;
        l.numerator *= b.denominator;
        l.denominator *= b.denominator;
        Fraction r  = b;
        r.numerator *= denominator;
        r.denominator *= denominator;
        Fraction result;
        result.numerator = l.numerator - r.numerator;
        result.denominator = l.denominator;
        return result.simplified();
    }
    const Fraction Fraction::operator-(const int64_t &n) {
        return *this - Fraction(n, 1);
    }
    const Fraction Fraction::operator/(const Fraction &b) {
        Fraction r = Fraction(b.denominator, b.numerator);
        return *this * r;
    }
    const Fraction Fraction::operator/(const int64_t &n) {
        return *this / Fraction(n, 1);
    }
    const Fraction Fraction::operator*(const Fraction &b) {
        return Fraction(numerator * b.numerator, denominator * b.denominator).simplified();
    }
    const Fraction Fraction::operator*(const int64_t &n) {
        return *this * Fraction(n, 1);
    }
    Fraction &Fraction::operator+=(const Fraction &b) {
        *this = *this + b;
        return *this;
    }
    Fraction &Fraction::operator+=(const int64_t &n) {
        return *this += Fraction(n, 1);
    }
    Fraction &Fraction::operator-=(const Fraction &b) {
        *this = *this - b;
        return *this;
    }
    Fraction &Fraction::operator-=(const int64_t &n) {
        return *this -= Fraction(n, 1);
    }
    Fraction &Fraction::operator/=(const Fraction &b) {
        *this = *this / b;
        return *this;
    }
    Fraction &Fraction::operator/=(const int64_t &n) {
        return *this /= Fraction(n, 1);
    }
    Fraction &Fraction::operator*=(const Fraction &b) {
        *this = *this * b;
        return *this;
    }
    Fraction &Fraction::operator*=(const int64_t &n) {
        return *this *= Fraction(n, 1);
    }
    Fraction &Fraction::operator=(const Fraction &b) {
        numerator = b.numerator;
        denominator = b.denominator;
        return *this;
    }
    Fraction &Fraction::operator=(const double &b) {
        std::ostringstream ss;
        ss << b;
        int l = ss.str().length(), i = 1;
        std::size_t pos = ss.str().find(".");
        if (pos == std::string::npos) {
            Fraction r(b, 1);
            *this = r;
            return *this;
        }
        while (true) {
            ss.str(ss.str().substr(1, l - i));
            if (ss.str().substr(0, 1).compare(".") == 0) {
                l = ss.str().length() - 1;
                break;
            }
            ++i;
        }
        int m = pow(10, l);
        double ip;
        int64_t d = m;
        double md = modf(b, &ip);
        if (b > 1) {
            d = round(d * md);
        } else {
            d *= b;
        }
        Fraction f(d, m);
        f.simplify();
        if (b > 1) {
            f.numerator = ((int)b * f.denominator) + f.numerator;
            f.simplify();
        }
        *this = f;
    }
    bool Fraction::operator==(const Fraction &b) {
        Fraction a = this->simplified(), f = b.simplified();
        return (a.numerator == f.numerator && a.denominator == f.denominator);
    }
    bool Fraction::operator==(const int64_t &n) {
        Fraction b(n, 1);
        return (numerator == b.numerator && denominator == b.denominator);
    }
    bool Fraction::operator!=(const Fraction &b) {
        return !(*this == b);
    }
    bool Fraction::operator!=(const int64_t &n) {
        Fraction b(n, 1);
        return !(*this == b);
    }
    bool Fraction::operator>(const Fraction &b) {
        return (compare(toDecimal(), b.toDecimal()) == 1);
    }
    bool Fraction::operator>(const int64_t &n) {
        Fraction b(n, 1);
        return *this > n;
    }
    bool Fraction::operator<(const Fraction &b) {
        return (compare(toDecimal(), b.toDecimal()) == -1);
    }
    bool Fraction::operator<(const int64_t &n) {
        Fraction b(n, 1);
        return *this < n;
    }
    bool Fraction::operator>=(const Fraction &b) {
        int8_t c = compare(toDecimal(), b.toDecimal());
        return (c == 1 || c == 0);
    }
    bool Fraction::operator>=(const int64_t &n) {
        Fraction b(n, 1);
        return *this >= n;
    }
    bool Fraction::operator<=(const Fraction &b) {
        int8_t c = compare(toDecimal(), b.toDecimal());
        return (c == -1 || c == 0);
    }
    bool Fraction::operator<=(const int64_t &n) {
        Fraction b(n, 1);
        return *this <= n;
    }
    std::ostream &operator<<(std::ostream &out, const Fraction &f) {
        out << "(" << f.numerator << "/" << f.denominator << ")";
        return out;
    }
    double Fraction::toDecimal() const {
        return (double)numerator / (double)denominator;
    }
}
